<?php
/**
 * The Template for displaying single posts.
 *
 * @package marlin
 */

get_header(); ?>

	<div class="col-md-8 site-main">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', 'single' ); ?>
		<?php endwhile; // end of the loop. ?>
	</div><!-- .site-main -->
     
<?php get_sidebar(); ?>

<div class = 'full advertisement'>
    <?php
        $args = new WP_Query( array( 'post_type' => 'advertisement','tag' => '728-x-90' ) );
        while($args->have_posts()) : $args->the_post();
        the_content();
        endwhile; ?>
    <?php wp_reset_postdata(); ?>
</div>   
<?php get_footer(); ?>