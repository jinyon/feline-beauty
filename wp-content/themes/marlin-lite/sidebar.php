<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package marlin-lite
 */
?>

<div class="col-md-4 sidebar">
    <aside id="sidebar">
        <?php if ( is_active_sidebar('sidebar') ) { dynamic_sidebar('sidebar'); } ?>
 
    <div class='left advertisement'>
        <?php
            $args = new WP_Query( array( 'post_type' => 'advertisement','tag' => '300-x-250' ) );
            while($args->have_posts()) : $args->the_post();
            the_content();
            endwhile; 
            ?> 
    </div>
    <div class='left advertisement'>
        <?php
            $args = new WP_Query( array( 'post_type' => 'advertisement','tag' => 'custom-ads-1' ) );
            while($args->have_posts()) : $args->the_post();
            the_content();
            endwhile; 
            ?>
    </div>
    <div class='left advertisement'>
        <?php
            $args = new WP_Query( array( 'post_type' => 'advertisement','tag' => 'custom-ads-2' ) );
            while($args->have_posts()) : $args->the_post();
            the_content();
            endwhile; 
            ?>
    </div>        
        
    </aside>
</div>