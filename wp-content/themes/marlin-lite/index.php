<?php
/**
 * The main template file.
 *
 * @package marlin-lite
 */

get_header(); ?>
<div class = 'center advertisement'>

<?php
    $args = new WP_Query( array( 'post_type' => 'advertisement','tag' => '728-x-90' ) );
    while($args->have_posts()) : $args->the_post();
    the_content();
    endwhile; ?> 
<?php wp_reset_postdata(); ?>

</div>
	<div class="col-md-8 site-main">
		<div id="main" class="vt-blog-standard">
			<?php
			if ( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post(); 
						/*
						* Include the post format-specific template for the content. If you want to
						* use this in a child theme, then include a file called called content-___.php
						* (where ___ is the post format) and that will be used instead.
						*/
						get_template_part( 'content', get_post_format() );?>
            
                    <?php if ($wp_query->current_post == 3):
                    $args = new WP_Query( array( 'post_type' => 'advertisement','tag' => '468-x-60' ) );
                    while($args->have_posts()) : $args->the_post();
                    ?>
                    <div class = 'center advertisement'>
                    <?php the_content(); ?>
                    </div>
                    <?php endwhile; ?>
                    <?php endif ?>
            <?php endwhile;
                    // Previous/next post navigation.
                    the_posts_pagination();
                
               else :
                    // If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );
					
               endif;
               ?>
		</div>
	</div><!-- site-main -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>