<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'felinebe_feline');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's_Wytd?7FrD<4+O+oQbeG@Z4R.5Y@^$oEaNPw^-m(sOPR8vsoXb7kBctg{ZYWD>:');
define('SECURE_AUTH_KEY',  'zuE=sFBd64bt|Vq;o5km))yxT>~|#q+IZw*?c/?vo,GdB$;?V>vVITBU1; ]aes)');
define('LOGGED_IN_KEY',    '[HHP97K~1%WNRv-j+>WFC)p5]xp>nmP2`v5eJ9&<U>*V/ef72MS1KCi~#}%qj^!5');
define('NONCE_KEY',        '5sUw!)t[8A5bihkr@.y!~,(oCjJ3CHbEa+Vw3/nTGxCt)%)o1K 8@Jf`yKAeZbg)');
define('AUTH_SALT',        'CCzQ22S|3pAV|-1JQjGE.<j{2to!J 0bF&3Nk8CUGdTVi(g#L`ExbA^vYP<^L5))');
define('SECURE_AUTH_SALT', 'S)D<e>s#n0mcL2CQc}!`4e~8-6j(A]&O1 +7[(ZuL0@mlHK 02$ldx=P]$it]Iu-');
define('LOGGED_IN_SALT',   '_QUMD2&Rns~.&0NU6=@?j,_FCV@2~|godCRtpHV2%Nn9ad59/6jQS{9>:d85 )c/');
define('NONCE_SALT',       'T9ng$qp6>p/67(I.8&1#H~$:45R^H{`7uIBO(&xxs=0)I(yI<<7U0!;)};D/=v)R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
